#!/usr/bin/python
# -*- coding: utf-8 -*-

from time import sleep
from OmegaExpansion import onionI2C # requires fixes for https://github.com/OnionIoT/i2c-exp-driver/issues/13
import os
import datetime
import re
import json
from valve import endWatering, startWatering
import threading


class Brain:

    def __init__(self, filejson, time, mod, duration):
        self.namefile = filejson
        self.starttime = time
        self.duration = duration
        self.mode = mod

        try:
            f = open(self.namefile)
            f.close()
        except Warning:
            print('Il file non esiste')

        if re.search('\d\d\:\d\d', self.starttime):
            pass
        else:
            print('Hai impostato un tempo di inizio non valido')

        if (self.mode == 1 or self.mode == 'auto' or self.mode == 'automatico' or self.mode == 'automatic'):
            self.mode = 'automatico'
        elif (self.mode == 2 or self.mode == 'man' or self.mode == 'manuale' or self.mode == 'manual'):
            self.mode = 'manuale'
        else:
            print('La modalità scelta non è valida')

        '''
            TODO: add list_hum and list_temp for backup
        '''

    def getTempAndHum(self):

        ''' It appends values of humidity and temperature into a list '''

        i2c = onionI2C.OnionI2C()
        i2c.write(0x40, [0xE5])
        sleep(0.1)
        h0, h1 = i2c.read(0x40, 2)
        i2c.write(0x40, [0xE0])
        sleep(0.1)
        t0, t1 = i2c.read(0x40, 2)

        value = {}

        value['hum'] = ((125.0 * (h0 * 256 + h1)) / 65536.0) - 6.0
        value['temp'] = ((175.72 * (t0 * 256 + t1)) / 65536.0) - 46.85
        now = datetime.datetime.now()
        value['time'] = '{}/{}'.format(now.hour, now.minute)

        self.save_data(value)
        return value

    def save_data(self, value):
        now = datetime.datetime.now()
        file = self.read_data('{}/{}/{}'.format(now.day, now.month, now.year))
        file["values"].append(value)
        '''
            TODO save json 
            TODO log()
        '''
        print('Data saved in {}'.format(self.namefile))

    def read_data(self, day=None):
        with open(self.namefile, 'r') as json_file:
            dictionary = json.load(json_file)
            if day == None:
                return dictionary
            else:
                if day in dictionary:
                    return dictionary[day]
                else:
                    dictionary[day] = {}


    def getValveTime(self, date):
        ''' It calculates the time ON for the water valve '''

        if self.mode == 'automatico':
            mean_temp = self.getMeanTemp(date)
            mean_hum = self.getMeanHum(date)
            ratio = mean_temp / mean_hum
            # Time in minutes
            valve_time = ratio * 60
            print('Valve time = {} minutes'.format(valve_time))
        elif self.mode == 'manuale':
            valve_time = self.duration

        now = datetime.datetime.now()
        file = self.read_data('{}/{}/{}'.format(now.day, now.month, now.year))
        file["duration"] = valve_time


    def irrigate(self):

        now = datetime.datetime.now()
        file = self.read_data('{}/{}/{}'.format(now.day, now.month, now.year))
        valve_time = file["duration"]
        t = threading.Timer(valve_time * 60, endWatering)
        startWatering()
        t.start()

    def getMeanTemp(self, day):
        dictionary = self.read_data(day)
        values = dictionary['values']
        len = len(values)
        for i in len:
            sum_temp += values[i]['temp']
        mean_temp = sum_temp/len         
        return mean_temp

    def getMeanHum(self, day):
        dictionary = self.read_data(day)
        values = dictionary['values']
        len = len(values)
        for i in len:
            sum_hum += values[i]['hum']

        mean_hum = sum_hum / len
        return mean_hum