#!/usr/bin/env python
# -*- coding: utf-8 -*-

import telepot
import requests
import re
allowed_users = ['the_real_fofila', 'SantGio']

def on_chat_message(msg):

    content_type, chat_type, chat_id = telepot.glance(msg)
    if content_type == 'text':
        name = msg["from"]["first_name"]
        user_name = msg["from"]["username"]
        txt = msg['text']

        if user_name in allowed_users:

            if '/temperatura' in txt:
                r = requests.get('http://192.168.1.11:5002/sensor/0')
                value = r.json()
                bot.sendMessage(chat_id, 'La temperatura attuale è {}'.format(value))
                bot.sendMessage(chat_id, 'ho ricevuto questo: %s' % txt)

            elif '/umidita' in txt:
                r = requests.get('http://192.168.1.11:5002/sensor/1')
                value = r.json()
                bot.sendMessage(chat_id, 'La temperatura attuale è {}'.format(value))
                bot.sendMessage(chat_id, 'ho ricevuto questo: %s' % txt)

            elif '/modo' in txt:
               if len(txt) == len('/modo'):
                    bot.sendMessage(chat_id, 'La modalità di utilizzo è %s' % modo)
               else:
                    modo = txt[5:]  # invia richiesta al server
                    modi = ['automatico', 'manuale']
                    if modo in modi:
                        bot.sendMessage(chat_id, 'Hai impostato il modo: %s' % modo)
                    else:
                        bot.sendMessage(chat_id, 'Hai sbagliato a scrivere, puoi impostare il modo automatico o manuale')

            elif '/tempoinizio' in txt:
                if len(txt) == len('/tempoinizio'):
                    bot.sendMessage(chat_id, "L'irrigazione partirà alle %s e durerà per minuti %s" % tempoinizio % durata)
                else:
                    tempoinizio = txt[12:]
                    if re.search('\d\d\:\d\d', tempoinizio): #invia richiesta al server
                        bot.sendMessage(chat_id, "Hai impostato l'inizio di irrigazione alle %s" % tempoinizio)

            elif '/durata' in txt:
                if len(txt) == len('\durata'):
                    bot.sendMessage(chat_id, "L'irrigazione durerà per %s" % durata)
                else:
                    durata = txt[6:]
                    if re.search('\d\d', durata): #invia richiesta al server
                        bot.sendMessage(chat_id, "Hai impostato la durata di irrigazione pari a  %s minuti" %durata)

            elif '/aiuto' in txt:

                bot.sendMessage(chat_id, 'I comandi disponibili sono:')
                bot.sendMessage(chat_id, '/temperatura: mostra la temperatura attuale')
                bot.sendMessage(chat_id, "/umidita: mostra l'umidità attuale")
                bot.sendMessage(chat_id, "/modo: indica se l'irrigazione è automatica o manuale")
                bot.sendMessage(chat_id, "/tempoinizio: mostra l'ora a cui partirà l'irrigazione e per quanto tempo")
                bot.sendMessage(chat_id, '/aiuto: mostra i possibili comandi')
                bot.sendMessage(chat_id, 'ho ricevuto questo: %s' % txt)

            else:
                bot.sendMessage(chat_id, 'Comando sbagliato!!!')
                bot.sendMessage(chat_id, 'I comandi disponibili sono:')
                bot.sendMessage(chat_id, '/aiuto')
                bot.sendMessage(chat_id, '/temperatura')
                bot.sendMessage(chat_id, '/umidita')
                bot.sendMessage(chat_id, '/modo')
                bot.sendMessage(chat_id, '/tempoinizio')

                bot.sendMessage(chat_id, 'ho ricevuto questo: %s' % txt)

TOKEN = '790101905:AAFHn8de3ovCXXOChgtt_Q_qrofunZIZUeE'

bot = telepot.Bot(TOKEN)
bot.message_loop(on_chat_message)

print 'Listening ...'

import time
while 1:
    time.sleep(10)