#!/usr/bin/python

from Si7020 import Brain
from flask import Flask, request
from flask_restful import Resource, Api
import sys
from time import sleep
import datetime
import json
import os

'''
  TODO: logger, timestamp
  TODO: valvola
  TODO: auth?
  TODO: sommare bot con main
  TODO: parametrizzare indirizzo IP
  TODO: guardare perché non salva
  TODO: json, creare nuovo elemento
  TODO: fare richiesta con chrono
  TODO: salvare durata, ora inizio su json

  modo  --> 1 auto
        --> 2 man
  
  sensori --> temp
          --> hum
          --> media_temp
          --> media_hum
          --> ora_i + min

  pass required
  motore  --> on
          --> off
          --> seth
'''

with open('config.json') as json_file:  
  data = json.load(json_file)

main = Brain(data['namefile'], data['starttime'], data['mode'], data['duration'])
app = Flask(__name__)
api = Api(app)

class Mode(Resource):
  def get(self, type_id):
    return main.mode
  def post(self, type_id):
    if(type_id == 1 or type_id == 'auto' or type_id == 'automatico' or type_id=='automatic'):
      tipo = 'automatico'
    elif (type_id == 2 or type_id == 'man' or type_id == 'manuale' or type_id=='manual'):
      tipo = 'manuale'
    else:
      tipo = 'automatico'
    # TODO log
    # TODO main.mode = tipo
    return 'Modo impostato {}'.format(type_id)

class Sensor(Resource):
  def get(self, sensor_type):
    now = datetime.datetime.now()
    time = '{}:{}'.format(now.hour, now.minute)
    result = main.getTempAndHum()
    if sensor_type == '0':
      return result['temp']
    elif sensor_type == '1':
      return result['hum']

class Valve(Resource):
  def get(self):
    return 'yes'
  def post(self, bool):
    return jsonify(result)
        

api.add_resource(Mode, '/mode/<type_id>') # Route_1
api.add_resource(Sensor, '/sensor/<sensor_type>') # Route_2
api.add_resource(Valve, '/valve/<bool>') # Route_3
os.system('python bot.py >> bot-log.txt')

if __name__ == '__main__':
  app.run(host= '192.168.1.11', port='5002')